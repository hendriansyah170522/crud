<link rel="stylesheet" href="style.css">
<body>
    <?php
    include "koneksi.php";

    $npm = $_GET['npm'];

    (isset($npm) && empty($npm)) ? header('location: index.php') : '';

    $query = "SELECT * FROM mahasiswa WHERE npm = $npm LIMIT 1";

    $hasil_query = mysqli_query($koneksi, $query);

    $data = mysqli_fetch_assoc($hasil_query);

    empty($data) ? header('location: index.php') : '';

    ?>

    <form method="POST" action="update.php?npm=<?=$npm; ?>">
        <h3>Form Edit</h3>
        <a href="index.php" type="button">Kembali</a>
        <table>
            <tr>
                <td>NPM</td>
                <td>:</td>
                <td><input type="text" name="npm" value="<?=$data['npm']; ?>" readonly></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td><input type="text" name="Nama" value="<?=$data['Nama']; ?>" required></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td><input type="text" name="Alamat" value="<?=$data['Alamat']; ?>" required></td>
            </tr>
            <tr>
                <td>Kelas</td>
                <td>:</td>
                <td><input type="text" name="Kelas" value="<?=$data['Kelas']; ?>" required></td>
            </tr>
            <tr>
                <td colspan="3">
                    <button type="submit" id="btn">Simpan</button>
                </td>
            </tr>
        </table>
    </form>
</body>